import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Router } from '@angular/router';

@Component({
  selector: 'app-qrcode',
  templateUrl: './qrcode.page.html',
  styleUrls: ['./qrcode.page.scss'],
})
export class QrcodePage implements OnInit {
  email: string;
  qrcode_liste = [];
  message: string;

  constructor(private firestore: AngularFirestore,
              private router: Router) { 
    this.email = localStorage.getItem('email');
  }

  ngOnInit() {
    this.listeQrCode();
  }

  listeQrCode() {
    this.firestore.firestore.collection('infoCommande').where('email_acheteur','==', this.email).where('livrer', '==', '').onSnapshot(
      (querySnapchot) => {
        
        if(querySnapchot.empty) {
          this.message = "Aucune commande en attente de scannge"
        }
        if(!querySnapchot.empty) {
          this.qrcode_liste = [];
          querySnapchot.forEach((doc) => {
            this.qrcode_liste.push(doc.data().cmdCode);
          })
        }
      })
  }

  voirQrCode(item) {
    console.log(item)
    localStorage.qrcode = item;
    this.router.navigate(['qrcode-vue']);
  }

}
