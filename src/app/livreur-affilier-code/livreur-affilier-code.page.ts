import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Router } from '@angular/router';

@Component({
  selector: 'app-livreur-affilier-code',
  templateUrl: './livreur-affilier-code.page.html',
  styleUrls: ['./livreur-affilier-code.page.scss'],
})
export class LivreurAffilierCodePage implements OnInit {
  code_validation: string;
  email: string;

  constructor(private firestore: AngularFirestore,
              private router: Router) { 
   this.email = localStorage.getItem('email');
  }

  ngOnInit() {
    this.firestore.firestore.collection('livreur_affilier').where('email', '==', this.email).onSnapshot(
      (querySnapchot) => {
        var compte_valider
        querySnapchot.forEach((doc) => {
          this.code_validation = doc.data().code_validation;
          compte_valider = doc.data().compte_valider;
        })
        if(compte_valider === 'oui') {
          this.router.navigate(['livreur-tabs']);
          alert('compte valider !!!');
        }
      }
    )
  }

}
