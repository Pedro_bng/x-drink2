import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { NotificationsVendeurPage } from './notifications-vendeur.page';

const routes: Routes = [
  {
    path: '',
    component: NotificationsVendeurPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class NotificationsVendeurPageRoutingModule {}
