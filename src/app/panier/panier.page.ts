import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Router } from '@angular/router';
import { LoadingController } from '@ionic/angular';
import { isBuffer } from 'node:util';
interface Cart {
  id: number;
  img: string;
  name: string;
  qty: number;
  amount: number;
}
@Component({
  selector: 'app-panier',
  templateUrl: './panier.page.html',
  styleUrls: ['./panier.page.scss'],
})

export class PanierPage implements OnInit {

  cart: Cart[] = [];
  total = 0;
  inputQty = false;
  nomStock = [];
  selectedDepot = [];
  qty: any;
  depotValideDoublons = [];
  selectedDepot2Doublons = [];
  selectedDepot2 = [];
  depotValides = [];
  distances = [];
  distanceMin: number;
  prix: any;
  depots = [];
  lth = 0;
  depotFinal: any;
  distance2 = [];
  distance2Min: number;
  btnPayer = false;
  nomStock1: any[];
  nomStock2: any[];
  precision: string;
  type: string;
  nomStock3: any[];
  nomStock4: any;
  nomStock5: any;
  nomStock6: any;
  nomStock7: any;
 

  constructor( public firestore: AngularFirestore,
               private router: Router,
               public loadingController: LoadingController) {
            
    /*this.firestore.collection('depot_marchand').snapshotChanges()
    .subscribe(dep => {
      dep.forEach(depx => {
        // tslint:disable-next-line:no-string-literal
        this.nomStock.push(depx.payload.doc.data()['nom_depot']);
      });
      console.log(this.nomStock);
    });*/

    this.type = localStorage.getItem('type');


    this.firestore.firestore.collection('vendeur').where('precision', '==', 'depot_marchand').onSnapshot(
      (querySnapchot) => {
        this.nomStock1 = [];
        querySnapchot.forEach((doc) => {
          this.nomStock1.push(doc.data().nom_depot)
        })
        console.log(this.nomStock1);
      }
    )

    this.firestore.firestore.collection('vendeur').where('precision', '==', 'distributeur_agreer').onSnapshot(
      (querySnapchot) => {
        this.nomStock2 = [];
        querySnapchot.forEach((doc) => {
          this.nomStock2.push(doc.data().distributeur_agreer)
        })
        console.log(this.nomStock2);
      }
    )

    this.firestore.firestore.collection('vendeur').where('type_distributeur', '==', 'sobebra').onSnapshot(
      (querySnapchot) => {
        this.nomStock3 = [];
        querySnapchot.forEach((doc) => {
          this.nomStock3.push(doc.data().distributeur_agreer)
        })
        console.log(this.nomStock3);
      }
    )

    this.firestore.firestore.collection('vendeur').where('type_distributeur', '==', 'libs').onSnapshot(
      (querySnapchot) => {
        this.nomStock4 = [];
        querySnapchot.forEach((doc) => {
          this.nomStock4.push(doc.data().distributeur_agreer)
        })
        console.log(this.nomStock4);
      }
    )

    this.firestore.firestore.collection('vendeur').where('type_distributeur', '==', 'liqueur_champagne').onSnapshot(
      (querySnapchot) => {
        this.nomStock5 = [];
        querySnapchot.forEach((doc) => {
          this.nomStock5.push(doc.data().distributeur_agreer)
        })
        console.log(this.nomStock5);
      }
    )

    this.firestore.firestore.collection('vendeur').where('type_distributeur', '==', 'jus_autres').onSnapshot(
      (querySnapchot) => {
        this.nomStock6 = [];
        querySnapchot.forEach((doc) => {
          this.nomStock6.push(doc.data().distributeur_agreer)
        })
        console.log(this.nomStock6);
      }
    )

    this.firestore.firestore.collection('vendeur').where('type_distributeur', '==', 'autres_grossistes').onSnapshot(
      (querySnapchot) => {
        this.nomStock7 = [];
        querySnapchot.forEach((doc) => {
          this.nomStock7.push(doc.data().distributeur_agreer)
        })
        console.log(this.nomStock7);
      }
    )
    

    

  }

  ngOnInit() {
  }

  ionViewWillEnter(){
    this.cart = JSON.parse(localStorage.getItem('cart')) || [];
    console.log('this.cart :>> ', this.cart);
    this.cart.forEach(element => {
      if(!element.amount){
        // element.amount=250
      }
      if (!element.qty){
        element.qty =1;
      }

     /* this.firestore.collection('depot_marchand').snapshotChanges()
        .subscribe( dep => {
          dep.forEach(depx => {
            // tslint:disable-next-line:no-string-literal
            console.log ('nomDepot:' + depx.payload.doc.data()['nom_depot'] );
            // tslint:disable-next-line:no-string-literal
            this.nomDepot = depx.payload.doc.data()['nom_depot'];
            console.log(this.nomDepot);
            // tslint:disable-next-line:no-string-literal
            console.log('latitude:' + depx.payload.doc.data()['latitude']  );
            // tslint:disable-next-line:no-string-literal
            console.log('longitude:' + depx.payload.doc.data()['longitude']  );

            this.firestore.collection(this.nomDepot).snapshotChanges()
            .subscribe(res => {
              res.forEach(resx => {
                // tslint:disable-next-line:no-string-literal
                if (element.name === resx.payload.doc.data()['nom_prod']) {
                  // tslint:disable-next-line:no-string-literal
                  if (element.qty < resx.payload.doc.data()['qty'] ) {
                    // tslint:disable-next-line:no-string-literal
                    console.log(resx.payload.doc.data()['nom_depot']);
                  }
                }
              });
            });
          } );
        });*/

    });
  }

  validerQuantite4() {
    //creation des differentes categories
    var categories = [];
    categories.push('sobebra', 'liqueur');
    console.log(categories);
    //classement des vendeurs par categories (en fonction des produits qu'ils vendent)
    var vendeur_sobebra = [];
    var vendeur_liqueur = [];
   // vendeur_sobebra = this.nomStock1
    //cinder la commande en plusieurs categories
    var cart_sobebra = [];
    var cart_liqueur = [];
    this.cart.forEach(element => {
      categories.forEach(categoriesx => {
        this.firestore.collection(categoriesx).snapshotChanges()
        .subscribe(nom_produits => {
          nom_produits.forEach(nom_produitsx => {
            // si le nom d'un produit se retrouve dans une categories
            // on l'ajoute au panier corespondant
            if(element.name === nom_produitsx.payload.doc.data()['nom_produit']) {
              //categorie sobebra
              if (categoriesx === 'sobebra') {
                //verifier le type de client avant de definir le vendeur
                if(this.type === 'client') {
                   vendeur_sobebra = this.nomStock1;
                   cart_sobebra.push(element);
                   console.log('categorie: sobebra')
                   console.log(cart_sobebra);
                   console.log(cart_liqueur);
                }
            
                if(this.type === 'vendeur') {
                  //verification du type de vendeur_acheteur
                  var email = localStorage.getItem('email');
                  this.firestore.firestore.collection('vendeur').where('email', '==', email ).get().then(
                    (querySnapchot) => {
                      var precision_vendeur_acheteur;
                      querySnapchot.forEach((doc) => {
                        precision_vendeur_acheteur = doc.data().precision
                      })
                      if(precision_vendeur_acheteur === 'depot_marchand') {
                        vendeur_sobebra =  this.nomStock3;
                        cart_sobebra.push(element);
                        console.log('categorie: sobebra')
                        console.log(cart_sobebra);
                        console.log(cart_liqueur);
                      }
                      if(precision_vendeur_acheteur === 'distributeur_agreer') {
                        // A revoir
                      }
                    }
                  )
                }
               
              }

              //categorie liqueur
              if(categoriesx === 'liqueur') {
                cart_liqueur.push(element);
                console.log('categorie: liqueur')
                console.log(cart_sobebra);
                console.log(cart_liqueur);
              }
            }
          })
        })
      })
    })
  }

  validerQuantite3(){
    this.presentLoading();
    if(this.type === 'client') {
      this.nomStock1.forEach(depx => {
        this.firestore.collection(depx).snapshotChanges()
        .subscribe(depx1 => {
          depx1.forEach(produit => {
            this.cart.forEach(element => {
              // tslint:disable-next-line:no-string-literal
              if (element.name === produit.payload.doc.data()['nom_prod'] && element.qty < produit.payload.doc.data()['qty']) {
                // tslint:disable-next-line:prefer-const
                let lat1 = localStorage.getItem('latitude');
                // tslint:disable-next-line:prefer-const
                let long1 = localStorage.getItem('longitude');
                // tslint:disable-next-line:no-string-literal
                const lat2 = produit.payload.doc.data()['latitude'];
                // tslint:disable-next-line:no-string-literal
                const long2 = produit.payload.doc.data()['longitude'];
                // tslint:disable-next-line:prefer-const
                let distance = this.getDistance([lat1, long1], [lat2, long2]);
                this.distances.push(distance);
  
                // tslint:disable-next-line:no-string-literal
                this.depots.push(produit.payload.doc.data()['nom_depot']);
                console.log(this.depots);
  
  
  
                this.selectedDepot.push({
                  // tslint:disable-next-line:no-string-literal
                  depot: produit.payload.doc.data()['nom_depot'],
                  // tslint:disable-next-line:no-string-literal
                  qty: produit.payload.doc.data()['qty'],
                  // tslint:disable-next-line:no-string-literal
                  article: produit.payload.doc.data()['nom_prod'],
                  distance: this.getDistance([lat1, long1], [lat2, long2]),
                  // tslint:disable-next-line:no-string-literal
                  prix_unitaire: produit.payload.doc.data()['prix_unitaire']
                });
                console.log(this.selectedDepot);
  
  
  
                this.depots.forEach( depot => {
                  // tslint:disable-next-line:prefer-const
                  let indices = [];
                  // tslint:disable-next-line:prefer-const
                  let idx = this.depots.indexOf(depot);
                  while (idx !== -1) {
                    indices.push(idx);
                    idx = this.depots.indexOf(depot, idx + 1);
                  }
                  console.log(indices);
                  this.lth = this.cart.length;
                  console.log(this.lth);
                  if (indices.length === this.lth) {
                    console.log(indices.length);
                    this.depotValideDoublons.push(depot);
                    this.depotValides = Array.from(new Set (this.depotValideDoublons));
                    console.log(this.depotValides);
                  }
                });

                this.selectedDepot2.forEach(selectedDepot2x => {
                  this.distance2.push(selectedDepot2x.distance);
                  this.distance2 = Array.from(new Set(this.distance2));
                  console.log(this.distance2.indexOf(0));
                  const elementasupprimer = this.distance2.indexOf(0);
                  console.log (elementasupprimer);
                  // this.distance2.splice(elementasupprimer);
                 // console.log(this.distance2);

                  //la plus petite distance

                  this.distance2Min = Math.min(...this.distance2);
                  console.log(this.distance2Min);

                  if (selectedDepot2x.distance === this.distance2Min) {
                    //recuperation du vendeur final et de sa precision
                    this.depotFinal = selectedDepot2x.depot;
                    this.precision = 'depot_marchand';
                    console.log(this.depotFinal);


                    this.firestore.collection(this.depotFinal).snapshotChanges()
                    .subscribe(depotfinalx => {
                      depotfinalx.forEach(prod => {
                        this.cart.forEach(element2 => {
                          // tslint:disable-next-line:no-string-literal
                          if (element2.name === prod.payload.doc.data()['nom_prod']) {
  
                this.depotValides.forEach(depotValide => {
                  this.selectedDepot.forEach(selectedDepotx => {
                    if (depotValide === selectedDepotx.depot) {
                      this.selectedDepot2Doublons.push(selectedDepotx);
                      this.selectedDepot2 = Array.from(new Set(this.selectedDepot2Doublons));
                    }
                    console.log(this.selectedDepot2);
                    console.log(this.distances);
                    this.distanceMin = Math.min(...this.distances);
                    console.log(this.distanceMin);
                  });
  
                  this.selectedDepot2.forEach(selectedDepot2x => {
                    this.distance2.push(selectedDepot2x.distance);
                    this.distance2 = Array.from(new Set(this.distance2));
                    console.log(this.distance2.indexOf(0));
                    const elementasupprimer = this.distance2.indexOf(0);
                    console.log (elementasupprimer);
                    // this.distance2.splice(elementasupprimer);
                   // console.log(this.distance2);
  
                    //la plus petite distance
  
                    this.distance2Min = Math.min(...this.distance2);
                    console.log(this.distance2Min);
  
                    if (selectedDepot2x.distance === this.distance2Min) {
                      //recuperation du vendeur final et de sa precision
                      this.depotFinal = selectedDepot2x.depot;
                      this.precision = 'depot_marchand';
                      console.log(this.depotFinal);
  
  
                      this.firestore.collection(this.depotFinal).snapshotChanges()
                      .subscribe(depotfinalx => {
                        depotfinalx.forEach(prod => {
                          this.cart.forEach(element2 => {

                            // tslint:disable-next-line:no-string-literal
                            if (element2.name === prod.payload.doc.data()['nom_prod']) {
                              // tslint:disable-next-line:no-string-literal
                              element2.amount = prod.payload.doc.data()['prix_unitaire'];
                              this.compute_total();
                              this.btnPayer = true;
                            }
                          });
                        });
                      });
                    }
                  });
  
                });
                
              } 
      
            });
          });
        });
      })
    }

    if(this.type === 'vendeur') {
      //verification de la precision du vendeur_acheteur
      var email = localStorage.getItem('email');
      this.firestore.firestore.collection('vendeur').where('email', '==', email).get().then(
        (querySnapchot) => {
          var precision_vendeur_acheteur;
          querySnapchot.forEach((doc) => {
            precision_vendeur_acheteur = doc.data().precision
          })
          //si vendeur_acheteur est depot_marchand
          if(precision_vendeur_acheteur === 'depot_marchand') {
            this.nomStock2.forEach(depx => {
              this.firestore.collection(depx).snapshotChanges()
              .subscribe(depx1 => {
                depx1.forEach(produit => {
                  this.cart.forEach(element => {
                    // tslint:disable-next-line:no-string-literal
                    if (element.name === produit.payload.doc.data()['nom_prod'] && element.qty < produit.payload.doc.data()['qty']) {
                      // tslint:disable-next-line:prefer-const
                      let lat1 = localStorage.getItem('latitude');
                      // tslint:disable-next-line:prefer-const
                      let long1 = localStorage.getItem('longitude');
                      // tslint:disable-next-line:no-string-literal
                      const lat2 = produit.payload.doc.data()['latitude'];
                      // tslint:disable-next-line:no-string-literal
                      const long2 = produit.payload.doc.data()['longitude'];
                      // tslint:disable-next-line:prefer-const
                      let distance = this.getDistance([lat1, long1], [lat2, long2]);
                      this.distances.push(distance);
        
                      // tslint:disable-next-line:no-string-literal
                      this.depots.push(produit.payload.doc.data()['nom_depot']);
                      console.log(this.depots);
        
        
        
                      this.selectedDepot.push({
                        // tslint:disable-next-line:no-string-literal
                        depot: produit.payload.doc.data()['nom_depot'],
                        // tslint:disable-next-line:no-string-literal
                        qty: produit.payload.doc.data()['qty'],
                        // tslint:disable-next-line:no-string-literal
                        article: produit.payload.doc.data()['nom_prod'],
                        distance: this.getDistance([lat1, long1], [lat2, long2]),
                        // tslint:disable-next-line:no-string-literal
                        prix_unitaire: produit.payload.doc.data()['prix_unitaire']
                      });
                      console.log(this.selectedDepot);
        
        
        
                      this.depots.forEach( depot => {
                        // tslint:disable-next-line:prefer-const
                        let indices = [];
                        // tslint:disable-next-line:prefer-const
                        let idx = this.depots.indexOf(depot);
                        while (idx !== -1) {
                          indices.push(idx);
                          idx = this.depots.indexOf(depot, idx + 1);
                        }
                        console.log(indices);
                        this.lth = this.cart.length;
                        console.log(this.lth);
                        if (indices.length === this.lth) {
                          console.log(indices.length);
                          this.depotValideDoublons.push(depot);
                          this.depotValides = Array.from(new Set (this.depotValideDoublons));
                          console.log(this.depotValides);
                        }
                      });
        
                      this.depotValides.forEach(depotValide => {
                        this.selectedDepot.forEach(selectedDepotx => {
                          if (depotValide === selectedDepotx.depot) {
                            this.selectedDepot2Doublons.push(selectedDepotx);
                            this.selectedDepot2 = Array.from(new Set(this.selectedDepot2Doublons));
                          }
                          console.log(this.selectedDepot2);
                          console.log(this.distances);
                          this.distanceMin = Math.min(...this.distances);
                          console.log(this.distanceMin);
                        });
        
                        this.selectedDepot2.forEach(selectedDepot2x => {
                          this.distance2.push(selectedDepot2x.distance);
                          this.distance2 = Array.from(new Set(this.distance2));
                          console.log(this.distance2.indexOf(0));
                          const elementasupprimer = this.distance2.indexOf(0);
                          console.log (elementasupprimer);
                          // this.distance2.splice(elementasupprimer);
                         // console.log(this.distance2);
        
                          //la plus petite distance
        
                          this.distance2Min = Math.min(...this.distance2);
                          console.log(this.distance2Min);
        
                          if (selectedDepot2x.distance === this.distance2Min) {
                            //recuperation du vendeur final et de sa precision
                            this.depotFinal = selectedDepot2x.depot;
                            this.precision = 'distributeur_agreer';
                            console.log(this.depotFinal);
        
        
                            this.firestore.collection(this.depotFinal).snapshotChanges()
                            .subscribe(depotfinalx => {
                              depotfinalx.forEach(prod => {
                                this.cart.forEach(element2 => {
                                  // tslint:disable-next-line:no-string-literal
                                  if (element2.name === prod.payload.doc.data()['nom_prod']) {
                                    // tslint:disable-next-line:no-string-literal
                                    element2.amount = prod.payload.doc.data()['prix_unitaire'];
                                    this.compute_total();
                                    this.btnPayer = true;
                                  }
                                });
                              });
                            });
                          }
                        });
        
                      });
                      
                    } 
            
                  });
                });
              });


            }

              
            } 
    
          });
        });
      });
    });
            }); 
          }
          //si vendeur_acheteur est distributeur_agreer
          if(precision_vendeur_acheteur === 'distributeur_agreer') {
            this.firestore.collection('sobebra').snapshotChanges()
            .subscribe(sobebra => {
              sobebra.forEach(prod => {
                this.cart.forEach(element => {
                  if(element.name === prod.payload.doc.data()['nom_produit']) {
                    element.amount = prod.payload.doc.data()['prix'];
                    this.compute_total();
                    this.btnPayer = true;
                  }
                })
              })
            })

          }
        }
      )

    }
    
  }

 /* validerQuantite2(item){
    console.log(this.nomStock);
    this.nomStock.forEach(depx => {
      this.firestore.collection(depx).snapshotChanges()
      .subscribe(depx1 => {
        depx1.forEach(produit => {
          // tslint:disable-next-line:no-string-literal
          console.log (produit.payload.doc.data()['nom_prod']);
          console.log(item.name);
          // tslint:disable-next-line:no-string-literal
          if (item.name === produit.payload.doc.data()['nom_prod'] && item.qty < produit.payload.doc.data()['qty']) {
             // tslint:disable-next-line:prefer-const
             let lat1 = localStorage.getItem('latitude');
             // tslint:disable-next-line:prefer-const
             let long1 = localStorage.getItem('longitude');
             // tslint:disable-next-line:no-string-literal
             const lat2 = produit.payload.doc.data()['latitude'];
             // tslint:disable-next-line:no-string-literal
             const long2 = produit.payload.doc.data()['longitude'];
             // tslint:disable-next-line:prefer-const
             let distance = this.getDistance([lat1, long1], [lat2, long2]);

             this.distances.push(distance);

             this.selectedDepot.push({
              // tslint:disable-next-line:no-string-literal
              depot: produit.payload.doc.data()['nom_depot'],
              // tslint:disable-next-line:no-string-literal
              qty: produit.payload.doc.data()['qty'],
              // tslint:disable-next-line:no-string-literal
              article: produit.payload.doc.data()['nom_prod'],
              distance: this.getDistance([lat1, long1], [lat2, long2]),
              // tslint:disable-next-line:no-string-literal
              prix_unitaire: produit.payload.doc.data()['prix_unitaire']
            });
             console.log(this.selectedDepot);
             this.distanceMin = Math.min(...this.distances);
             console.log(this.distanceMin);

             this.selectedDepot.forEach(selectedDepotx => {
              if (this.distanceMin === selectedDepotx.distance) {
                this.depotValide = selectedDepotx.depot;
                console.log(this.depotValide);
                item.amount = selectedDepotx.prix_unitaire;
                
              }
             });
           }
        });
      });
    });
  }*/

 /* validerQuantite(item){

    this.cart.forEach(element => {
      if (element.id === item.id) {
        this.firestore.collection('depot_marchand').snapshotChanges()
        .subscribe( dep => {
          dep.forEach(depx => {
            // tslint:disable-next-line:no-string-literal
            console.log ('nomDepot:' + depx.payload.doc.data()['nom_depot'] );
            // tslint:disable-next-line:no-string-literal
            this.nomStock.push(depx.payload.doc.data()['nom_depot']);
            console.log(this.nomStock);
            // tslint:disable-next-line:no-string-literal
            console.log('latitude:' + depx.payload.doc.data()['latitude']  );
            // tslint:disable-next-line:no-string-literal
            console.log('longitude:' + depx.payload.doc.data()['longitude']  );
          } );

          this.nomStock.forEach(nomstockx => {
            this.firestore.collection(nomstockx).snapshotChanges()
            .subscribe(result => {
              result.forEach(resultx => {
                // tslint:disable-next-line:no-string-literal
                if (item.name === resultx.payload.doc.data()['nom_prod']) {
                  // tslint:disable-next-line:no-string-literal
                  if (item.qty < resultx.payload.doc.data()['qty']) {
                    // tslint:disable-next-line:no-string-literal
                    console.log(resultx.payload.doc.data()['nom_depot']);
                    this.selectedDepot.push(
                      // tslint:disable-next-line:no-string-literal
                       resultx.payload.doc.data()['nom_depot'],
                    );
                    console.log(this.selectedDepot);
                    this.selectedDepot.forEach(selectedDepotx => {
                      this.firestore.firestore.collection('depot_marchand').where('nom_depot', '==', selectedDepotx).onSnapshot(
                        (querySnapshot) => {
                          // tslint:disable-next-line:prefer-const
                          let lat1 = localStorage.getItem('latitude');
                          // tslint:disable-next-line:prefer-const
                          let long1 = localStorage.getItem('longitude');
                         // tslint:disable-next-line:prefer-const
                          let lat2;
                         // tslint:disable-next-line:prefer-const
                          let long2;
                          // tslint:disable-next-line:variable-name
                          let nom_depot2;
                          let distance;
                          querySnapshot.forEach((doc) => {
                             nom_depot2 = doc.data().nom_depot;
                             lat2 = doc.data().latitude;
                             long2 = doc.data().longitude;
                             // calcul de la distance
                             distance = this.getDistance([lat1, long1], [lat2, long2]);
                             console.log('distance:' + distance, 'nom_depot:' + nom_depot2);
                           });
                          this.distances.push(distance);
                          this.depotValide.push({
                            nom_depot: nom_depot2,
                            // tslint:disable-next-line:object-literal-shorthand
                            distance: distance,
                          });
                          console.log(this.depotValide);
                          console.log(this.distances);


                          this.distanceMin = Math.min(...this.distances);
                          console.log(this.distanceMin);

                          this.depotValides.forEach(depotValidex => {
                            if (depotValidex.distance === this.distanceMin ) {
                              console.log(depotValidex.nom_depot);
                              this.firestore.firestore.collection(depotValidex.nom_depot).where('nom_prod', '==', item.name).onSnapshot(
                                // tslint:disable-next-line:no-shadowed-variable
                                (querySnapshot) => {
                                 querySnapshot.forEach((doc) => {
                                     this.prix = doc.data().prix_unitaire;
                                     item.amount = this.prix;
                                   });

                               });
                            }
                          });
                       });
                    });
                   }
                }
              } );
            });
          } );
        });
      }
    });
  }*/

 /* inputChange(event, item){
    console.log('event :>> ', event);
    // tslint:disable-next-line:radix
    item.qty = parseInt(event);

    this.firestore.collection('depot_marchand').snapshotChanges()
    .subscribe( dep => {
      dep.forEach(depx => {
        // tslint:disable-next-line:no-string-literal
        console.log ('nomDepot:' + depx.payload.doc.data()['nom_depot'] );
        // tslint:disable-next-line:no-string-literal
        this.nomStock.push(depx.payload.doc.data()['nom_depot']);
        console.log(this.nomStock);
        // tslint:disable-next-line:no-string-literal
        console.log('latitude:' + depx.payload.doc.data()['latitude']  );
        // tslint:disable-next-line:no-string-literal
        console.log('longitude:' + depx.payload.doc.data()['longitude']  );
      } );

      this.nomStock.forEach(nomstockx => {
        this.firestore.collection(nomstockx).snapshotChanges()
        .subscribe(result => {
          result.forEach(resultx => {
            // tslint:disable-next-line:no-string-literal
            if (item.name === resultx.payload.doc.data()['nom_prod']) {
              // tslint:disable-next-line:no-string-literal
              if (item.qty < resultx.payload.doc.data()['qty']) {
                // tslint:disable-next-line:no-string-literal
                console.log(resultx.payload.doc.data()['nom_depot']);
                this.selectedDepot.push(
                  // tslint:disable-next-line:no-string-literal
                   resultx.payload.doc.data()['nom_depot'],
                );
                console.log(this.selectedDepot);

               }
            }
          } );
        });
      } );


    });

    this.compute_total();
  }*/


  increase(item: Cart){
    if (item.qty){
      item.qty = item.qty + 1;
    }
    else{
      item.qty = 2;
    }
    this.compute_total()
  }
  decrease(item: Cart){
    if (!(item.qty - 1 <= 0)){
      item.qty = item.qty - 1;
    }

    this.compute_total();
  }

  delete(item){
    this.cart.forEach((element, i) => {
      if (element.id === item.id){
        console.log('i :>> ', i);
        this.cart.splice(i, 1);
        this.compute_total();
      }
    });

  }


  compute_total(){
    this.total = 0;
    this.cart.forEach(element => {
      this.total = this.total + element.qty * element.amount;
    });
    localStorage.setItem('cart2', JSON.stringify(this.cart));
  }

  getDistance(origin, destination) {
    // return distance in meters
    // tslint:disable-next-line:prefer-const
    let lon1 = this.toRadian(origin[1]);
    // tslint:disable-next-line:prefer-const
    let lat1 = this.toRadian(origin[0]);
    // tslint:disable-next-line:prefer-const
    let lon2 = this.toRadian(destination[1]);
    // tslint:disable-next-line:prefer-const
    let lat2 = this.toRadian(destination[0]);

    // tslint:disable-next-line:prefer-const
    let deltaLat = lat2 - lat1;
    // tslint:disable-next-line:prefer-const
    let deltaLon = lon2 - lon1;

    // tslint:disable-next-line:prefer-const
    let a = Math.pow(Math.sin(deltaLat / 2), 2) + Math.cos(lat1) * Math.cos(lat2) * Math.pow(Math.sin(deltaLon / 2), 2);
    // tslint:disable-next-line:prefer-const
    let c = 2 * Math.asin(Math.sqrt(a));
    // tslint:disable-next-line:prefer-const
    let EARTH_RADIUS = 6371;
    return c * EARTH_RADIUS * 1000;
  }

  toRadian(degree) {
    return degree * Math.PI / 180;
  }

  async presentLoading() {
    const loading = await this.loadingController.create({
      cssClass: 'my-custom-class',
      message: 'Recherche en cours ...',
      duration: 2000
    });
    await loading.present();

    const { role, data } = await loading.onDidDismiss();
    console.log('Loading dismissed!');
  }

  payer() {
    this.router.navigate(['livraison-client']);
    localStorage.depotFinal = this.depotFinal;
    localStorage.precision_vendeur = this.precision;
    this.cart = [];
    this.total = 0;
  }

}
