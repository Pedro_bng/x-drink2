import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { RestitutionCasierPage } from './restitution-casier.page';

describe('RestitutionCasierPage', () => {
  let component: RestitutionCasierPage;
  let fixture: ComponentFixture<RestitutionCasierPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RestitutionCasierPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(RestitutionCasierPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
