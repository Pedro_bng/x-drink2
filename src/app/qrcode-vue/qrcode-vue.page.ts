import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-qrcode-vue',
  templateUrl: './qrcode-vue.page.html',
  styleUrls: ['./qrcode-vue.page.scss'],
})
export class QrcodeVuePage implements OnInit {
  public myAngularxQrCode: string = null;

  constructor() {
    
   }

  ngOnInit() {
    this.myAngularxQrCode = localStorage.getItem('qrcode');
    console.log(this.myAngularxQrCode);
  }

}
