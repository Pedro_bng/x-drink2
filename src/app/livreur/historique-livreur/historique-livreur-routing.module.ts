import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HistoriqueLivreurPage } from './historique-livreur.page';

const routes: Routes = [
  {
    path: '',
    component: HistoriqueLivreurPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class HistoriqueLivreurPageRoutingModule {}
