import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CodeVendeurPageRoutingModule } from './code-vendeur-routing.module';

import { CodeVendeurPage } from './code-vendeur.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CodeVendeurPageRoutingModule
  ],
  declarations: [CodeVendeurPage]
})
export class CodeVendeurPageModule {}
