import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CodeVendeurPage } from './code-vendeur.page';

const routes: Routes = [
  {
    path: '',
    component: CodeVendeurPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CodeVendeurPageRoutingModule {}
